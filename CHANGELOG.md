# Changelog

#### PressureGauge-1.1 - 08/08/24:
    remove state machine for reading Pressure

#### PressureGauge-1.0 - 04/03/19:
    Initial Revision in GIT
